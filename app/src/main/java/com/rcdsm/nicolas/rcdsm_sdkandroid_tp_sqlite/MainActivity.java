package com.rcdsm.nicolas.rcdsm_sdkandroid_tp_sqlite;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends Activity implements View.OnClickListener, TextWatcher {

	protected MaBaseDeDonnees dbHelper;
	protected SQLiteDatabase db;
	protected PersonnesCursorAdapter ca;

	protected ListView lv;

	protected Button bt_add;
	protected EditText et_searchKey;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		bt_add = (Button) findViewById(R.id.bt_add);
		et_searchKey = (EditText) findViewById(R.id.et_searchKey);
		lv = (ListView) findViewById(R.id.listView);

		bt_add.setOnClickListener(this);
		et_searchKey.addTextChangedListener(this);

		dbHelper = new MaBaseDeDonnees(this);

		db = dbHelper.getWritableDatabase();

		String[] listeDesChamps = {"_id", "nom", "prenom"};

		Cursor cursor = db.query("personnes", listeDesChamps, null, null, null, null, "nom");

		ca = new PersonnesCursorAdapter(this, cursor);

		lv.setAdapter(ca);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
			if(resultCode == 1){
				Toast.makeText(this, "Ajout réussi", Toast.LENGTH_LONG).show();
				onTextChanged(et_searchKey.getText().toString(), 0, 0, 0);
			}else{
				Toast.makeText(this, "Echec de l'ajout", Toast.LENGTH_LONG).show();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == bt_add.getId()){
			Intent i = new Intent(this, ActivityAjout.class);
			this.startActivityForResult(i, 1);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		String[] listeDesChamps = {"_id", "nom", "prenom"};

		String condition;

		if(!s.equals("")){
			condition = "nom like '%" + s + "%' or prenom like '%" + s + "%'";
		}else{
			condition = null;
		}

		ca.setCursor(db.query("personnes", listeDesChamps, condition, null, null, null, "nom"));
		ca.notifyDataSetChanged();
	}

	@Override
	public void afterTextChanged(Editable s) {

	}
}
