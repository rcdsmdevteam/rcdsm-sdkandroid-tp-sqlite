package com.rcdsm.nicolas.rcdsm_sdkandroid_tp_sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Nicolas on 12/9/2014.
 */
public class MaBaseDeDonnees extends SQLiteOpenHelper {
	final static public String DATABASE_NAME = "mybdd.db";
	final static public int DATABASE_VERSION = 1;

	public MaBaseDeDonnees(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "create table personnes (_id integer primary key autoincrement, nom text, prenom text, age integer, sexe text)";

		try{
			db.execSQL(sql);
		}catch (SQLiteException e){
			Log.e("SQL", "Erreur lors de la création de la table personnes", e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}
