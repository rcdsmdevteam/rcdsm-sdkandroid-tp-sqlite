package com.rcdsm.nicolas.rcdsm_sdkandroid_tp_sqlite;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioGroup;


public class ActivityAjout extends Activity implements View.OnClickListener {

	protected EditText et_nom;
	protected EditText et_prenom;
	protected NumberPicker np_age;
	protected RadioGroup rbg_sexe;
	protected Button bt_ajouter;

	protected MaBaseDeDonnees dbHelper;
	protected SQLiteDatabase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity_ajout);

		et_nom = (EditText) findViewById(R.id.et_nom);
		et_prenom = (EditText) findViewById(R.id.et_prenom);
		np_age = (NumberPicker) findViewById(R.id.np_age);
		rbg_sexe = (RadioGroup) findViewById(R.id.rbg_sexe);
		bt_ajouter = (Button) findViewById(R.id.bt_ajouter);

		bt_ajouter.setOnClickListener(this);

		np_age.setMaxValue(150);
		np_age.setMinValue(0);

		dbHelper = new MaBaseDeDonnees(this);

		db = dbHelper.getWritableDatabase();
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.bt_ajouter){
			try{
				ContentValues values = new ContentValues();
				values.put("nom", et_nom.getText().toString());
				values.put("prenom", et_prenom.getText().toString());
				values.put("age", np_age.getValue());

				if(rbg_sexe.getCheckedRadioButtonId() == R.id.rb_homme){
					values.put("sexe", "M");
				}else{
					values.put("sexe", "F");
				}
				db.insert("personnes", null, values);
				this.setResult(1);
				finish();
			} catch (Exception e){
				this.setResult(0);
				finish();
			}
		}
	}
}
