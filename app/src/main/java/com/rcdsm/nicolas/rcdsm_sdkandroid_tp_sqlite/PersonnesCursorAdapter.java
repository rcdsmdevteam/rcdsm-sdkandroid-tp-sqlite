package com.rcdsm.nicolas.rcdsm_sdkandroid_tp_sqlite;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Nicolas on 12/11/2014.
 */
public class PersonnesCursorAdapter extends BaseAdapter {
	protected Context context;
	protected Cursor cursor;

	protected View itemView;
	protected TextView lb_id;
	protected TextView lb_nom;
	protected TextView lb_prenom;
	protected Button bt_supprimer;
	protected Button bt_modifier;

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}

	public PersonnesCursorAdapter(Context context, Cursor cursor) {
		this.context = context;
		this.cursor = cursor;

		this.itemView = null;
	}


	@Override
	public int getCount() {
		return this.cursor.getCount();
	}

	@Override
	public Object getItem(int position) {
		String[] row;
		row = new String[this.cursor.getColumnCount()];

		this.cursor.moveToPosition(position);

		for(int i = 0; i < this.cursor.getColumnCount(); i++){
			row[i] = this.cursor.getString(i);
		}

		return row;
	}

	@Override
	public long getItemId(int position) {
		this.cursor.moveToPosition(position);

		return Long.parseLong(this.cursor.getString(this.cursor.getColumnIndex("_id")));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater.from(this.context));

		this.itemView = inflater.inflate(R.layout.listview_item, null);
		final View refindableView = this.itemView;

		this.cursor.moveToPosition(position);

		this.lb_id = (TextView) this.itemView.findViewById(R.id.lb_id);
		this.lb_nom = (TextView) this.itemView.findViewById(R.id.lb_nom);
		this.lb_prenom = (TextView) this.itemView.findViewById(R.id.lb_prenom);
		this.bt_modifier = (Button) this.itemView.findViewById(R.id.bt_modifier);
		this.bt_supprimer = (Button) this.itemView.findViewById(R.id.bt_supprimer);

		this.lb_id.setText(this.cursor.getString(this.cursor.getColumnIndex("_id")));
		this.lb_nom.setText(this.cursor.getString(this.cursor.getColumnIndex("nom")));
		this.lb_prenom.setText(this.cursor.getString(this.cursor.getColumnIndex("prenom")));

		this.bt_modifier.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i("clicked", "modif " + ((TextView) refindableView.findViewById(R.id.lb_id)).getText().toString());

			}
		});

		this.bt_supprimer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i("clicked", "supprimer " + ((TextView) refindableView.findViewById(R.id.lb_id)).getText().toString());
			}
		});

		return this.itemView;
	}


}
